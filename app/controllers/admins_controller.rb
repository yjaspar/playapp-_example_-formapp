class AdminsController < ApplicationController
    require 'facebook'
    before_filter :authorize

# GET /admins
# GET /admins.json
def index
    @admins = Admin.all
    @_admin = Admin.new

    render :index, :layout => 'admin_layout'
end

def notifications
    @subscribes = Subscribe.all
    render :notifications, :layout => 'admin_layout'
end

# GET /admins/new
# GET /admins/new.json
def new
    @admin = Admin.new

    respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @admin }
    end
end

# GET /admins/1/edit
def edit
    @admin = Admin.find(params[:id])
end

# POST /admins
# POST /admins.json
def create
    
    @admin = Admin.new({:firstname => params['firstname'], :lastname => params['lastname'], :fb_userid => params['fb_userid']})
    @admin.save
    respond_to do |format|
        format.html { redirect_to admins_url, notice: 'Admin was successfully created.' }
        format.json { render json: @admin, status: :created, location: @admin }
    end
end

# PUT /admins/1
# PUT /admins/1.json
def update
    @admin = Admin.find(params[:id])

    respond_to do |format|
        if @admin.update_attributes(params[:admin])
            format.html { redirect_to admins_url, notice: 'Admin was successfully updated.' }
            format.json { head :no_content }
        else
            format.html { render action: "edit" }
            format.json { render json: @admin.errors, status: :unprocessable_entity }
        end
    end
end

def push

end

# DELETE /admins/1
# DELETE /admins/1.json
def destroy
    @admin = Admin.find(params[:id])
    @admin.destroy

    respond_to do |format|
        format.html { redirect_to admins_url, notice: 'Admin was successfully deleted.' }
        format.json { head :no_content }
    end
end
end
