class ApplicationController < ActionController::Base
	protect_from_forgery

	private
	def authorize
		unless Admin.find_by_id(session[:user_id])
			redirect_to(:controller => "subscribes", :action => "index")
		end
	end
end
