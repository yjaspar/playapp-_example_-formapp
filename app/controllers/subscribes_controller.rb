class SubscribesController < ApplicationController
    require 'facebook'
    before_filter :authorize, :except => [:index, :create, :get_by_fbid]

    # GET /subscribes
    # GET /subscribes.json
    def index
        @request = params[:signed_request] ? Facebook::Parsor.signed_request(params[:signed_request], Settings.facebook.app_secret) : session[:request]
        @admin = Admin.find_by_fb_userid(@request["user_id"])
        if @admin
            session[:user_id] = @admin.id
            session[:request] = @request
        end
        
        respond_to do |format|
            format.html # index.html.erb
        end
    end

    def destroy
        @subscribe = Subscribe.find(params[:id])
        @subscribe.destroy
        redirect_to action: 'list'
    end

    def create
        params[:subscribe][:device] = request.env['mobvious.device_type'].to_s
        puts params[:subscribe]
        @subscribe = Subscribe.new(params[:subscribe])

        @subscribe.save
        respond_to do |format|
            format.json { render json: @subscribe }
        end
    end

    def list
        @subscribes = Subscribe.all
        respond_to do |format|
            format.xls { send_data @subscribes.to_xls, disposition: 'attachment', :filename => 'Subscribes.xls' }
            format.html {render :layout => 'admin_layout'}
        end
    end

    # GET /subscribes/getbyfbid
    # GET /subscribes/getbyfbid.json
    def get_by_fbid
        @subscribe = Subscribe.find_by_fb_userid(params[:id])
        respond_to do |format|
            format.json { render json: @subscribe }
        end
    end

    def device_percentage
        @req = Subscribe.select('`device`, COUNT( `id` ) as count').group('`device`')
        puts @req.to_sql
        puts @req
        respond_to do |format|
            format.json { render json: @req }
        end
    end

    def get_participants_per_month
        df = params[:date_from].split('-')
        from = DateTime.new(df[2].to_i, df[1].to_i, df[0].to_i,0,0,0)
        
        dt = params[:date_to].split('-')
        to = DateTime.new(dt[2].to_i, dt[1].to_i, dt[0].to_i, 23,59,59)
        
        @ppm = Subscribe.where("`created_at` <= '"+to.strftime("%Y-%m-%d %H:%M:%s")+"' AND `created_at` >= '"+from.strftime("%Y-%m-%d %H:%M:%s")+"'")
        respond_to do |format|
            format.json { render json: @ppm }
        end
    end

    # GET /subscribes/getbyfbid
    # GET /subscribes/getbyfbid.json
    def stats
        @subscribes = Subscribe.all
        render :stats, :layout => 'admin_layout'
    end  
end
