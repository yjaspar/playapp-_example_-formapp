class Formapp.Models.Subscribe extends Backbone.Model
  	paramRoot: 'subscribe'

	defaults:
	    firstname: null
	    lastname: null
	    email: null
	    birthdate: null
	    use_conditions: null
	    fb_userid: null

class Formapp.Collections.SubscribesCollection extends Backbone.Collection
	model: Formapp.Models.Subscribe
	url: '/subscribes'