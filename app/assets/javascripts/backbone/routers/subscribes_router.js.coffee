class Formapp.Routers.SubscribesRouter extends Backbone.Router
    initialize: (options) ->
        console.log options
        @subscribes or= new Formapp.Collections.SubscribesCollection()
        @_options = options

    routes:
        "index"    : "index"
        "form"     : "dispForm"
        "social"   : "social"
        "error"    : "error"
        ".*"        : "index"

    index: ->
        self = @
        @subscribes.fetch {
            url: "/subscribes/getbyfacebookid/#{self._options.request.user_id}.json",
            success: (data, xhr) ->
                console.log data
                if data.length == 0 then self.dispIndex() else self.social()
            }

    dispForm: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeIn 'fast', () ->
            FB.api 'me', (data) ->
                self.view = new Formapp.Views.Subscribes.NewView(data: data, collection: self.subscribes)
                $("#app center").fadeOut 'fast', () ->
                    $('#app div').html(self.view.render().el)

    dispIndex: ->
        self = @
        @view = new Formapp.Views.Subscribes.IndexView(@_options)
        $("#app center").fadeOut 'fast', () ->
            $('#app div').html(self.view.render().el)

    error: ->
        @view = new Formapp.Views.Subscribes.ErrorView(@_options, @subscribes)
        $("#app div").html(@view.render().el)

    social: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            self.view = new Formapp.Views.Subscribes.SocialView(config: self._options, collection: self.subscribes)
            $("#app div").html(self.view.render().el)
