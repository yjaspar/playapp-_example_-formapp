Formapp.Views.Subscribes ||= {}

class Formapp.Views.Subscribes.SocialView extends Backbone.View
    template: JST["backbone/templates/subscribes/social"]
    events: {
        'click #btn_invitation': 'invitation'
        'click #btn_publication': 'share'
    }

    initialize: =>
        console.log @options

    share: () ->
        FB.ui
            method: 'feed',
            name: @options.config.config.facebook.app_title,
            link: 'https://www.facebook.com/thebetapage/app_'+@options.config.config.facebook.app_default_page_id,
            picture: @options.config.config.facebook.app_thumbnail,
            caption: @options.config.config.facebook.app_caption,
            description: @options.config.config.facebook.app_description

    invitation: () ->
        FB.ui {method: 'apprequests', message: @options.config.config.facebook.app_request_msg}

    render: ->
        console.log @options.collection.models[0].toJSON()
        @$el.html @template @options.collection.models[0].toJSON()
        return @
