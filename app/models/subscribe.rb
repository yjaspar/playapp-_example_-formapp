class Subscribe < ActiveRecord::Base
  attr_accessible :birthdate, :email, :firstname, :lastname, :use_conditions, :fb_userid, :device
  
  # validates :birthdate, format: { with: /[0-9]{2}\/[0-9]{2}\/[0-9]{4}/i, message: "Date format is DD/MM/YYYY"}
  # validates :email, format: {with: {/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, message: "Wrong email format"}
  # validates :fb_userid, uniqueness: true
  # validates :firstname, :lastname, :fb_userid, presence: true
end
