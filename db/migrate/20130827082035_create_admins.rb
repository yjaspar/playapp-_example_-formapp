class CreateAdmins < ActiveRecord::Migration
  def change
    create_table :admins do |t|
      t.string :fb_userid
      t.string :firstname
      t.string :lastname

      t.timestamps
    end
  end
end
