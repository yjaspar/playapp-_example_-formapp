class CreateSubscribes < ActiveRecord::Migration
  def change
    create_table :subscribes do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.datetime :birthdate
      t.boolean :use_conditions
      t.string  :fb_userid
      t.string :device
      t.timestamps
    end
  end
end
