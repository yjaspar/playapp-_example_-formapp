require 'test_helper'

class SubscribeTest < ActiveSupport::TestCase
	test "Should not subscription without argument"
		subscribe = Subscribe.new
	  	assert !subscribe.save, "Subscription saving without params"
	end
end
